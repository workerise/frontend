// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

/**
 * Takes an object where the key is the `data-cy` for the field
 * and the value is the value you want to be type in.
 */
export const userData = {
  id: 1,
  username: 'river',
  email: 'river@workerise.org',
}

export const companyData = {
  id: 1,
  name: 'Workerise',
}

Cypress.Commands.add('fillForm', (fieldValues) => {
  // Fill out form
  Object.entries(fieldValues).forEach((entry) => {
    const [dataCy, value] = entry
    cy.get(`[data-cy="${dataCy}"]`).clear().type(value)
  })
})

Cypress.Commands.add('signup', (customFieldValues = {}) => {
  cy.intercept('POST', '/api/initialize-company', {
    body: {
      user: {
        id: 1,
        username: 'river',
      },
      company: {
        id: 1,
        name: 'workerise',
      },
    },
    statusCode: 201,
  }).as('initializeCompany')

  const defaultFieldValues = {
    companyNameField: 'workerise',
    usernameField: 'river',
    emailField: 'test@workerise.org',
    passwordField: 'BOSCOBOSCO',
    confirmPasswordField: 'BOSCOBOSCO',
  }

  const fieldValues = {
    ...defaultFieldValues,
    ...customFieldValues,
  }

  cy.visit('/signup')

  cy.fillForm(fieldValues)

  cy.get(`[data-cy="submitButton"]`).click()
})

Cypress.Commands.add('login', (customFieldValues = {}) => {
  cy.intercept('POST', '/login/', {
    body: {
      token: '1234abcd',
    },
    statusCode: 200,
  }).as('login')

  cy.intercept('GET', '/user-data/', {
    body: {
      user: {
        ...userData,
        company: companyData,
      },
    },
    statusCode: 200,
  }).as('user')

  const defaultFieldValues = {
    usernameField: 'river',
    passwordField: 'BOSCOBOSCO',
  }

  const fieldValues = {
    ...defaultFieldValues,
    ...customFieldValues,
  }

  cy.visit('/login')

  cy.fillForm(fieldValues)

  cy.get(`[data-cy="submitButton"]`).click()
})

Cypress.Commands.add('loginAndVisit', (url) => {
  cy.login()
  cy.url().should('not.include', '/login')
  cy.visit(url)
})

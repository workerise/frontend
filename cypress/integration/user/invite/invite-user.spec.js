import { userData } from '../../support/commands'
describe('Invite user', () => {
  beforeEach(() => {
    cy.intercept('GET', '/api/users/', {
      body: [userData],
      statusCode: 200,
    }).as('getUsers')
  })

  it('a user can invite a colleague', () => {
    // Invited users
    const invitedUsersResponses = [
      [],
      [
        {
          id: 1,
          email: 'custom@email.com',
        },
      ],
    ]
    cy.intercept('GET', '/api/invited-users/', (req) =>
      req.reply(invitedUsersResponses.shift())
    ).as('getInvitedUsers')

    // Invite request response
    cy.intercept('Post', '/api/invite-user', {
      body: {
        email: 'custom@email.com',
      },
      statusCode: 201,
    }).as('inviteUser')

    // Navigate
    cy.loginAndVisit('/company')
    cy.wait(['@getUsers', '@getInvitedUsers'])

    // Check no invited user section
    cy.get('[data-cy="invitedColleaguesSection"]').should('not.exist')

    const formValues = {
      emailField: 'custom@email.com',
    }
    cy.fillForm(formValues)
    cy.get('[data-cy="submitInviteButton"]').click()

    cy.wait(['@inviteUser'])

    // A new user appears in the invited user list
    cy.get('[data-cy="invitedColleaguesSection"]')
      .should('exist')
      .within(() => {
        cy.contains('custom@email.com')
      })
  })

  it('a user can cancel an invite', () => {
    // Invited users
    const invitedUsersResponses = [
      [
        {
          id: 1,
          email: 'custom@email.com',
        },
      ],
      [],
    ]
    cy.intercept('GET', '/api/invited-users/', (req) =>
      req.reply(invitedUsersResponses.shift())
    ).as('getInvitedUsers')

    // Delete response
    cy.intercept('DELETE', '/api/invited-users/1', {
      statusCode: 204,
    })

    // Navigate
    cy.loginAndVisit('/company')
    cy.wait(['@getUsers', '@getInvitedUsers'])

    cy.get('[data-cy="invitedColleaguesSection"]')
      .should('exist')
      .within(() => {
        cy.get('[data-cy="cancelInviteButton-1"]').click()
      })

    cy.get('[data-cy="invitedColleaguesSection"]').should('not.exist')
  })

  describe('shows an error alert', () => {
    it('if email invalid', () => {
      cy.intercept('Post', '/api/invite-user', {
        body: { email: ['Enter a valid email address.'] },
        statusCode: 400,
      }).as('inviteUser')
      cy.loginAndVisit('/company')
      cy.wait(['@getUsers'])
      cy.fillForm({
        emailField: 'aaaaaaaaaaaa',
      })
      cy.get('[data-cy="submitInviteButton"]').click()
      cy.wait(['@inviteUser'])
      // Check for error
      cy.get('[data-cy="errorAlert"]').should('exist')
    })

    it.skip('if email already in system', () => {}) // TODO: Build this feature in backend
  })

  describe('shows validation errors', () => {
    it('when required fields are empty', () => {
      cy.loginAndVisit('/company')
      cy.get(`[data-cy="submitInviteButton"]`).click()
      cy.contains('Required')
    })
  })
})

describe('Accept Invite page', () => {
  it('sends user to 404 page if no user', () => {
    cy.intercept('GET', 'api/invited-users/this-is-an-example-uuid', {
      statusCode: 405,
    })

    cy.visit('/accept-invite?id=this-is-an-example-uuid')

    cy.url().should('include', '/404')
  })

  describe('when there is an invited user with the id in params', () => {
    beforeEach(() => {
      // Get user
      cy.intercept('GET', 'api/invited-users/this-is-an-example-uuid', {
        statusCode: 200,
        body: {
          id: 1,
          company_name: 'Workerise',
          email: 'some@email.com',
        },
      })

      cy.visit('/accept-invite?id=this-is-an-example-uuid')
    })

    it('user can accept an invite', () => {
      // Accept invite
      cy.intercept('POST', '/api/accept-invite', {
        statusCode: 201,
      })

      cy.fillForm({
        usernameField: 'laura_jane_grace',
        passwordField: 'secretpassword',
        confirmPasswordField: 'secretpassword',
      })

      cy.get('[type="submit"]').click()

      // Should redirect to login screen with username
      cy.url().should('include', '/login?username=laura_jane_grace')
    })

    describe('shows error alert', () => {
      it('when accept invite fails', () => {
        cy.intercept('POST', '/api/accept-invite', {
          statusCode: 500,
        })

        cy.fillForm({
          usernameField: 'laura_jane_grace',
          passwordField: 'secretpassword',
          confirmPasswordField: 'secretpassword',
        })

        cy.get('[type="submit"]').click()

        cy.get('[data-cy="error-alert"]').should('exist')
      })
    })

    describe('shows validation errors', () => {
      it('when required fields are empty', () => {
        cy.fillForm({
          passwordField: 'secretpassword',
          confirmPasswordField: 'secretpassword',
        })

        cy.get('[type="submit"]').click()

        cy.contains('Required').should('exist')
      })

      it('when passwords do not match', () => {
        cy.fillForm({
          usernameField: 'laura_jane_grace',
          passwordField: 'secretpassworP',
          confirmPasswordField: 'secretpassword',
        })

        cy.get('[type="submit"]').click()

        cy.contains('Does not match.').should('exist')
      })
    })
  })
})

describe('Login page', () => {
  it('user can log in successfully', () => {
    cy.login()
    // Check next page
    cy.url().should('include', '/tasks')
    cy.url().should('not.include', '/login')
  })

  describe('shows an alert on page load', () => {
    it('when query param `username` is in the URL', () => {
      cy.visit('/login?username=river')
      // Check alert text exists on page
      cy.get('[data-cy="queryParamAlert"]').should('exist')
    })
  })

  describe('shows an error alert', () => {
    it('if credentials incorrect', () => {
      cy.intercept('POST', '/api/login', {
        statusCode: 401,
      })

      cy.visit('/login')

      const fieldValues = {
        usernameField: 'river',
        passwordField: 'uwu_uwu_uwu',
      }

      cy.fillForm(fieldValues)

      cy.get('[data-cy="submitButton"]').click()

      cy.get('[data-cy="errorAlert')
      cy.url().should('include', '/login')
    })
  })

  describe('shows validation errors', () => {
    it('when required fields are empty', () => {
      cy.visit('/login')

      cy.get(`[data-cy="submitButton"]`).click()

      cy.contains('Required')
      cy.url().should('include', '/login')
    })
  })
})

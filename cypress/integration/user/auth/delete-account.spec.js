describe('Delete User', () => {
  beforeEach(() => {
    cy.login()
    cy.visit('/user/settings')
    cy.get('[data-cy="delete-account-section-activator"]').click()
  })

  it('user can successfully delete account', () => {
    cy.intercept('DELETE', '/api/users/1/', {
      statusCode: 204,
    })

    cy.fillForm({
      confirmDeleteField: 'river',
    })

    cy.get('[type="submit"]').click()

    cy.url().should('include', '/login')
  })

  describe('shows validation errors', () => {
    it('when required fields are empty', () => {
      cy.get('[type="submit"]').click()
      cy.contains('Required')
      cy.url().should('include', '/user/settings')
    })
  })
})

const changePasswordData = {
  oldPassword: 'BOSCOBOSCO',
  newPassword: 'OCSOBOCSOB',
}

describe('Change Password', () => {
  beforeEach(() => {
    cy.login()
    cy.visit('/user/settings')
    cy.get('[data-cy="change-password-section-activator"]').click()
  })

  it('user can successfully change password', () => {
    cy.intercept('POST', '/change-password/', {
      statusCode: 200,
    })

    cy.get('[data-cy="oldPasswordField"').within(() => {
      cy.get('input').clear().type(changePasswordData.oldPassword)
    })

    cy.get('[data-cy="newPasswordSection"').within(() => {
      cy.fillForm({
        passwordField: changePasswordData.newPassword,
        confirmPasswordField: changePasswordData.newPassword,
      })
    })

    cy.get('[type="submit"]').click()

    // Should show success alert
    cy.get('[data-cy="change-password-success-alert"]').should('exist')
  })

  it('shows an error when the current password is incorrect', () => {
    cy.intercept('POST', '/change-password/', {
      statusCode: 403,
    })

    cy.get('[data-cy="oldPasswordField"').within(() => {
      cy.get('input').clear().type('wrong password')
    })

    cy.get('[data-cy="newPasswordSection"').within(() => {
      cy.fillForm({
        passwordField: changePasswordData.newPassword,
        confirmPasswordField: changePasswordData.newPassword,
      })
    })

    cy.get('[type="submit"]').click()

    // Should show error alert
    cy.get('[data-cy="error-alert"]').should('exist')
  })
})

describe('Signup page', () => {
  it('user can sign up and create company successfully', () => {
    cy.signup()
    // Check next page
    cy.url().should('include', '/login?username=river')
  })

  describe('shows error alert', () => {
    it('when company with name exists', () => {
      cy.intercept('POST', '/api/initialize-company', {
        body: { name: ['Company with this name exists.'] },
        statusCode: 400,
      }).as('initializeCompany')

      cy.visit('/signup')

      cy.signup()

      cy.get(`[data-cy="submitButton"]`).click()

      // Does not change page
      cy.url().should('include', '/signup')
      cy.contains('Company with this name exists.')
    })
  })

  describe('shows validation errors', () => {
    it('when required fields are empty', () => {
      cy.visit('/signup')

      cy.get(`[data-cy="submitButton"]`).click()

      // Does not change page
      cy.url().should('include', '/signup')
      cy.contains('Required')
    })

    it('when passwords do not match', () => {
      cy.visit('/signup')

      const fieldValues = {
        companyNameField: 'workerise',
        usernameField: 'river',
        emailField: 'test@workerise.org',
        passwordField: 'BOSCOBOSCO',
        confirmPasswordField: 'diffentPasswordEntirely',
      }

      cy.fillForm(fieldValues)

      cy.get(`[data-cy="submitButton"]`).click()

      // Does not change page
      cy.url().should('include', '/signup')
      cy.contains('Does not match.')
    })
  })
})

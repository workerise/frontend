const newUserData = {
  username: 'newusername',
  email: 'new@email.com',
}

describe('Edit User Details', () => {
  beforeEach(() => {
    cy.login()
    cy.visit('/user/settings')
    cy.get('[data-cy="editDetailsButton"]').click()
  })

  // it('submit button is disabled if values are the same', () => {})

  it('user can successfully edit username and email', () => {
    cy.intercept('PATCH', '/api/users/1/', {
      statusCode: 200,
      body: {
        username: newUserData.username,
        email: newUserData.email,
      },
    })

    cy.fillForm({
      editUsernameField: newUserData.username,
      editEmailField: newUserData.email,
    })

    cy.get('[type="submit"]').click()

    // Should show success alert
    cy.get('[data-cy="edit-details-success-alert"]').should('exist')

    // Should no longer show submit button
    cy.get('[type="submit"]').should('not.exist')
  })

  describe('shows validation errors', () => {
    it('when required fields are empty', () => {
      // Delete values
      cy.get('[data-cy="editUsernameField"').clear()

      cy.get('[type="submit"]').click()

      cy.contains('Required')

      cy.get('[data-cy="edit-details-success-alert"]').should('not.exist')
    })
  })

  describe('shows error alert', () => {
    it('when username already exists', () => {
      cy.intercept('PATCH', '/api/users/1/', {
        statusCode: 400,
        body: { username: ['User with this username already exists'] },
      })

      cy.fillForm({
        editUsernameField: newUserData.username,
        editEmailField: newUserData.email,
      })
      cy.get('[type="submit"]').click()
      cy.contains('User with this username already exists')
      cy.url().should('include', '/user/settings')
    })
  })
})

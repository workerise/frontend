import dotenv from 'dotenv'
dotenv.config()

export default {
  // Disable server-side rendering (https://go.nuxtjs.dev/ssr-mode)
  ssr: false,

  // Target (https://go.nuxtjs.dev/config-target)
  target: 'server',

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'Workerise',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    'nuxt-i18n',
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
    '@nuxtjs/proxy',
  ],

  router: {
    middleware: ['auth'],
  },

  axios: {
    baseUrl: process.env.WORKERISE_ENDPOINT,
  },
  proxy: {
    '/api': {
      target: process.env.WORKERISE_ENDPOINT,
      pathRewrite: { '^/api': '' },
    },
  },

  auth: {
    strategies: {
      local: {
        token: {
          property: 'token',
          type: 'Token',
        },
        user: {
          property: 'user',
        },
        endpoints: {
          login: {
            url: '/login/',
            method: 'POST',
          },
          user: {
            url: '/user-data/',
            method: 'GET',
          },
          logout: false,
        },
      },
    },
    // watchLoggedIn: true,
    redirect: {
      home: '/tasks',
      login: '/login',
      logout: '/login',
      redirect: '/tasks',
    },
  },

  // Vuetify module configuration (https://go.nuxtjs.dev/config-vuetify)
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      light: true,
      dark: false,
      themes: {
        light: {
          secondary: '#282828',
          accent: '#E2E2E2',
          warning: '#F2C94C',
          error: '#FF4646',
          success: '#1DBE71',
        },
      },
    },
  },

  i18n: {
    locales: [
      {
        code: 'en-GB',
        file: 'en-GB.json',
        name: 'English',
      },
      {
        code: 'de',
        file: 'de.json',
        name: 'Deutsch',
      },
    ],
    lazy: true,
    langDir: 'locale/',
    defaultLocale: 'en-GB',
    strategy: 'no_prefix',
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {},
}

export interface APIErrorResponseBody {
  [key: string]: string[]
}

/**
 * Backend Models
 */

//  Company
export interface Company {
  id: number
  name: string
}

// Users
export interface User {
  id: number
  username: string
  email: string
}

export interface UserData extends User {
  // Used by /user-data/ endpoint, sets Auth framework $auth.user
  company: Company
}

export interface InvitedUser {
  id: string
  email: string
  company: number
  company_name: string
}

// Tasks
export interface BackendTask {
  id: number
  key: string
  required: boolean
  completed: boolean
  completed_date: string // ISO String '2021-09-11'
  due_date: string
  meeting_time: Date // ISO Time String
}

export interface ContentfulTaskFields {
  key: string
  title: string
  content: any // Contentful rich text object
}

type JoinedTask = BackendTask & ContentfulTaskFields

// Resources
interface ContentfulResourceFields {
  title: string
  description: string
  content: any // Contentful rich text object
}

export default () => {}

// this.$t('forms.validation.required')
export const required = (e: string) => !!e

// this.$t('forms.validation.minLength')
export const minLength = (e: string, length: number = 8) =>
  e && e.length >= length

// this.$t('forms.validation.match')
export const match = (e: string, checkAgainst: string) => checkAgainst === e

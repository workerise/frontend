import { createClient } from 'contentful'

// use default environment config for convenience
// these will be set via `env` property in nuxt.config.js
const config = {
  space: process.env.NUXT_ENV_CONTENTFUL_SPACE_ID as string,
  accessToken: process.env.NUXT_ENV_CONTENTFUL_ACCESS_TOKEN as string,
}

// export `createClient` to use it in page components
export default {
  createClient() {
    return createClient(config)
  },
}

import fetchMock from 'fetch-mock'
import { authFetch } from './workerise'

describe('Workerise api service', () => {
  describe('authFetch()', () => {
    it('returns the body of a successfully resolved HTTP response', async () => {
      const respBody = {
        user: {
          id: 1,
          username: 'river',
        },
      }

      fetchMock.once('/api/login', {
        status: 200,
        body: respBody,
      })

      const body = await authFetch({
        url: '/api/login',
        method: 'POST',
        body: {
          username: 'river',
          password: 'BOSCO',
        },
      })
      // @ts-ignore
      expect(body).toEqual(respBody)
      const request = fetchMock.calls('/api/login')[0][1]
      // @ts-ignore
      expect(request?.headers['Authorization']).toEqual(undefined)
    })

    it('sets the token in the header of the request if provided', async () => {
      fetchMock.once('/api/users/', {
        method: 'POST',
        status: 200,
        body: [],
      })

      await authFetch({
        url: '/api/users/',
        method: 'POST',
        token: 'Token 12345',
        body: {
          username: 'river',
          password: 'BOSCO',
        },
      })
      // Check request made with header
      const request = fetchMock.calls('/api/users/')[0][1]
      // @ts-ignore
      expect(request?.headers['Authorization']).toEqual('Token 12345')
    })

    it('can override content type with custom headers if provided', async () => {
      fetchMock.once('/api/users/1234', {
        method: 'GET',
        status: 200,
        body: [],
      })

      await authFetch({
        url: '/api/users/1234',
        method: 'GET',
        headers: { 'Content-Type': 'text/xml' },
      })
      // Check request made with header
      const request = fetchMock.calls('/api/users/1234')[0][1]
      // @ts-ignore
      expect(request?.headers['Content-Type']).toEqual('text/xml')
    })

    describe('throws an error with the correct message when request not successful', () => {
      afterEach(() => {
        fetchMock.restore()
      })
      it('when the response has no body', async () => {
        fetchMock.once('/test-url', {
          status: 400,
        })

        try {
          await authFetch({
            url: '/test-url',
          })
        } catch (err) {
          // @ts-ignore
          expect(err.message).toEqual('Error making request')
        }
      })

      it('when the response has an object with a list of errors', async () => {
        fetchMock.once('/test-url', {
          status: 400,
          body: {
            email: ['Email address invalid'],
          },
        })

        try {
          await authFetch({
            url: '/test-url',
          })
        } catch (err) {
          // @ts-ignore
          expect(err.message).toEqual('Email address invalid')
        }
      })

      it('when the response is an object with a string', async () => {
        fetchMock.once('/test-url', {
          status: 400,
          body: {
            no_description: 'Incorrect credentials',
          },
        })

        try {
          await authFetch({
            url: '/test-url',
          })
        } catch (err) {
          // @ts-ignore
          expect(err.message).toEqual('Incorrect credentials')
        }
      })
    })
  })
})

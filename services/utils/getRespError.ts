import { APIErrorResponseBody } from '@/global.types'

export default (respBody: any) => {
  // If there is an error message from the backend
  const errorMessages = Object.entries(respBody as APIErrorResponseBody).map(
    // Flatten error list
    (entry) => {
      const [key, value] = entry
      // If a string
      if (typeof value === 'string') return [key, value]
      // If an array
      if (typeof value === 'object' && value.length > 0) return [key, value[0]]
      return []
    }
  )

  return errorMessages[0][1] || 'errors.generic'
}

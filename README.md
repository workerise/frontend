# Workerise Frontend

This is a Nuxt.js Single Page Application for the Workerise frontend.

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## Application Documentation

### External services

External services and their access codes are configured in .env. The required keys are include in `.env_template`. Copy this file to `.env` and set the variables to the values provided.

#### Workerise API

The path to the API is set in the env variable `WORKERISE_ENDPOINT`. The default in the env template file is `http://localhost:8000/`, which is the default for the Python backend running locally. When using localhost, the backend must be running locally on port 8000.

#### Contentful

To access the content that is hosted on Contentful, you need the Contenful space ID and access token. Then you should create a `.env` file with the following variables:

```
NUXT_ENV_CONTENTFUL_SPACE_ID=<contentful_space_id>
NUXT_ENV_CONTENTFUL_ACCESS_TOKEN=<contentful_access_token>
```

### Auth

User Next Auth's default `local` auth option (JWT).

It's configured that all routes require auth, but you can turn off the requirement per route by adding:

```js
auth: false,
```

to the component in the pages directory.

#### Token Authentication

If any request is made to the API that returns a 401 or 403 status denoting the tokens are incorrect, the user should be logged out and their token deleted on their client.

## Testing

- Jest - `npm run test`
- Cypress - `npm run cypress:open`

## Deployment

Deployed as an SSR application on Vercel.

### Vercel Docs

- [Vercel Builder](https://github.com/nuxt/vercel-builder)
